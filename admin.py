import asyncio
from discord.ext import commands
import discord
import subprocess

class Admin(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.conn = bot.conn
		self.cur = bot.cur

	async def cog_check(self, ctx):
	    return ctx.author.id == 193874867324583936

	async def cog_command_error(self, ctx, error):
		if isinstance(error, commands.CheckFailure):
			await ctx.send("Sorry, I'll only do that for anonezumi!")
		else:
			await ctx.send("Oops, there was an error:\n\n" + str(error))

	@commands.command(name="eval")
	async def _eval(self, ctx, *, code):
		"""Just a simple Eval command! Owner ownly."""
		await ctx.send(eval(code))

	@commands.command()
	async def die(self, ctx):
		"""Kills the bot process. Don't worry, I can always restart! Owner only."""
		await ctx.send("See you soon!")
		self.conn.commit()
		self.conn.close()
		await self.bot.logout()

	@commands.command()
	async def say(self, ctx, ch: discord.TextChannel, *, msg):
		"""Sends whatever you want, wherever you want! Owner only."""
		await ch.send(msg)

	@commands.command()
	async def update(self, ctx):
		"""Pulls from Git. Owner only."""
		process = await asyncio.create_subprocess_exec("git", "pull", stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout, stderr = await process.communicate()

		await ctx.send("Alright, pulled from Git! Here's the result:```\n{}\n{}```".format(stdout.decode(), stderr.decode()))

	@commands.command()
	async def reload(self, ctx, cog):
		"""Reloads a cog. Owner only."""
		self.bot.reload_extension(cog)
		await ctx.send("I reloaded {} for you!".format(cog))

	@die.error
	async def die_error(self, ctx, error):
		if isinstance(error, commands.CheckFailure):
			await ctx.send("You would really ask me to do that? I'm hurt... ;-;")
		else:
			await ctx.send("Oops, there was an error:\n\n" + str(error))

def setup(bot):
	bot.add_cog(Admin(bot))