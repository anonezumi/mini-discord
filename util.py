import asyncio
from discord.ext import commands

class Util(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.conn = bot.conn
		self.cur = bot.cur

	async def cog_command_error(self, ctx, error):
		await ctx.send("Oops, there was an error:\n\n" + str(error))

	@commands.command()
	async def about(self, ctx):
		await ctx.send(
"""**__About me!__**

I'm Mini, a bot made by anonezumi#9912. I'm meant to replace logbot, an old bot used for MiniTWOWs and other purposes, that logged messages between channels. I'm also their new general-purpose bot! If you have an idea for a feature for me, you can suggest it in my discussion channel on their personal server.

Here's a link to the server where you can discuss me, suggest features, submit bug reports, and get the newest updates! https://discord.gg/auuFwmQ

Here's a link that lets you add me to your own server! https://discord.com/oauth2/authorize?client_id=735722998841212928&scope=bot

Here's a link to a guide on how to use my blacklist feature. https://docs.google.com/document/d/1xx_WHSO1nDzq9OYgJDjkGdhJRRlXr2obOjjZvoPo6Eo/edit?usp=sharing

If you want help with my commands, use m.help.
"""
)

def setup(bot):
	bot.add_cog(Util(bot))