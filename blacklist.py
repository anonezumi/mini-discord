import asyncio
from discord.ext import commands
import discord
from random import choices
import re

async def has_manage_server(ctx):
	return (not isinstance(ctx.channel, discord.DMChannel)) and ctx.author.guild_permissions.manage_guild

class Blacklist(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.conn = bot.conn
		self.cur = bot.cur
		self.spoiler_re = re.compile(r"\|\|.*?\|\|")
		self.to_update = []

	async def cog_command_error(self, ctx, error):
		await ctx.send("Oops, there was an error:\n\n" + str(error))

	def get_blacklist(self, server):
		members = [m.id for m in self.bot.get_guild(server).members]
		self.cur.execute("SELECT * FROM blacklist_disabled WHERE server=?", (server,))
		disabled = [i[1] for i in self.cur.fetchall()]
		self.cur.execute("SELECT * FROM blacklist WHERE server IN (?,?)", (0, server))
		return [i for i in self.cur.fetchall() if not (i[0] in disabled) and (i[1] in members or not i[1])]

	def get_phrases(self, server):
		bl = self.get_blacklist(server)
		phrases = []
		for item in bl:
			self.cur.execute("SELECT * FROM autodetect WHERE blacklist_item=?", (item[0],))
			phrases += self.cur.fetchall()
		self.cur.execute("SELECT * FROM autodetect_disabled WHERE server=?", (server,))
		disabled = [p[1] for p in self.cur.fetchall()]
		for phrase in phrases:
			if phrase[0] in disabled:
				phrases.remove(phrase)
		return [p[1] for p in phrases]

	async def update_messages(self, guilds):
		for guild in guilds:
			self.cur.execute("SELECT * FROM blacklist_messages WHERE server=?", (guild,))
			prev_messages = self.cur.fetchall()
			for msg in prev_messages:
				todelete = await self.bot.get_channel(msg[1]).fetch_message(msg[2])
				await todelete.delete()
			self.cur.execute("DELETE FROM blacklist_messages WHERE server=?", (guild,))
			self.cur.execute("SELECT * FROM servers WHERE id=?", (guild,))
			chid = self.cur.fetchall()[0][3]
			if not chid: return
			channel = self.bot.get_channel(chid)
			bl = self.get_blacklist(guild)
			triggers = sorted([i for i in bl if not i[4]], key=lambda x: x[2].lower())
			squicks = sorted([i for i in bl if i[4]], key=lambda x: x[2].lower())
			messages = []
			message = "This is a list of triggers and squicks for the members of this server. Avoid mentioning these without putting a content warning using ||spoiler tags|| around the sensitive content. Triggers should be respected everywhere, while squicks are specific to a user and should be avoided while they are in the conversation.\n\nTriggers:\n||"
			for item in triggers:
				message += "[id: `{}`] {}\n".format(item[0], item[2])
				if len(message) > 1500:
					messages.append(message[0:-1] + "||")
					message = ""
			messages.append(message[0:-1] + "||")
			message = "Squicks:\n||"
			for item in squicks:
				message += "[id: `{}`] {} (<@{}>)\n".format(item[0], item[2], item[1])
				if len(message) > 1500:
					messages.append(message[0:-1] + "||")
					message = ""
			messages.append(message[0:-1] + "||")
			for msg in messages:
				sent = await channel.send(msg, allowed_mentions=discord.AllowedMentions(everyone=False, users=False, roles=False))
				self.cur.execute("INSERT INTO blacklist_messages(server,channel,message) VALUES (?,?,?)", (guild, chid, sent.id))
			self.conn.commit()

	def generate_id(self, phrase):
		while True:
			newid = "".join([chr(i) for i in choices(range(97, 123), k=5)])
			if phrase:
				self.cur.execute("SELECT * FROM autodetect WHERE id=?", (newid,))
			else:
				self.cur.execute("SELECT * FROM autodetect WHERE id=?", (newid,))
			if not self.cur.fetchall():
				return newid

	@commands.group()
	async def blacklist(self, ctx):
		"""Commands to manage the blacklist feature, for cataloguing the server members' triggers and squicks, or more simply, discussion topics that should be avoided. Here's a full guide on how to use the feature: https://docs.google.com/document/d/1xx_WHSO1nDzq9OYgJDjkGdhJRRlXr2obOjjZvoPo6Eo/edit?usp=sharing"""
		if ctx.invoked_subcommand is None:
			await ctx.send("The blacklist command has several subcommands. Use m.help blacklist for more information.")

	@blacklist.command(name="channel")
	@commands.check(has_manage_server)
	async def blacklist_channel(self, ctx, ch: discord.TextChannel):
		"""Set the channel where the message containing the list of server members' triggers is posted. Will automatically send the message. Takes the channel as an argument. Requires Manage Server."""
		if ch.guild != ctx.guild:
			await ctx.send("Ooh, sneaky... that isn't in this server!")
			return
		self.cur.execute("UPDATE servers SET blacklist_channel=? WHERE id=?", (ch.id, ctx.guild.id))
		await self.update_messages([ctx.guild.id])

	@blacklist.command()
	@commands.check(has_manage_server)
	async def orphaned_item(self, ctx, *, name):
		"""Add a blacklist item to the server which does not have an owner. Useful for importing blacklists and stating server-wide rules. Takes a blacklist item name. Requires Manage Server."""
		self.cur.execute("INSERT INTO blacklist(id, owner, name, server, squick) VALUES (?,?,?,?,?)", (self.generate_id(False), 0, name, ctx.guild.id, False))
		self.conn.commit()
		await ctx.send("Alright! Blacklist item added with name {}.".format(name))
		await self.update_messages([ctx.guild.id])

	@blacklist.command()
	@commands.check(has_manage_server)
	async def disable_item(self, ctx, item):
		"""Disable a specific blacklist item on the server. Takes the 5-letter item id. Requires Manage Server."""
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		if not self.cur.fetchall():
			await ctx.send("That's not a valid blacklist item.")
			return
		self.cur.execute("SELECT * FROM blacklist_disabled WHERE server=? AND id=?", (ctx.guild.id, item))
		if self.cur.fetchall():
			await ctx.send("That item is already disabled on this server.")
			return
		self.cur.execute("INSERT INTO blacklist_disabled(server,id) VALUES (?,?)", (ctx.guild.id, item))
		self.conn.commit()
		await ctx.send("Got it! I disabled that item for you, I'll resend the blacklist message now.")
		await self.update_messages([ctx.guild.id])

	@blacklist.command()
	@commands.check(has_manage_server)
	async def enable_item(self, ctx, item):
		"""Re-enable a specific blacklist item on the server. Takes the 5-letter item id. Requires Manage Server."""
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		if not self.cur.fetchall():
			await ctx.send("That's not a valid blacklist item.")
			return
		self.cur.execute("SELECT * FROM blacklist_disabled WHERE server=? AND id=?", (ctx.guild.id, item))
		if not self.cur.fetchall():
			await ctx.send("That item isn't disabled on this server.")
			return
		self.cur.execute("DELETE FROM blacklist_disabled WHERE server=? AND id=?", (ctx.guild.id, item))
		self.conn.commit()
		await ctx.send("Got it! I re-enabled that item for you, I'll resend the blacklist message now.")
		await self.update_messages([ctx.guild.id])

	@blacklist.command()
	@commands.check(has_manage_server)
	async def strike(self, ctx, message: discord.Message, item):
		"""Delete a message for violating the blacklist, informing the sender of their mistake. Takes the message id or link, and the 5-letter item id. Requires Manage Server."""
		user = message.author
		await message.delete()
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		items = self.cur.fetchall()
		itemname = None
		if items:
			itemname = items[0][2]
			await ctx.send("I deleted that message for you. I'll tell the author that they violated the ||{}|| blacklist item.".format(itemname))
			await message.author.send("Your message was deleted for violating the ||{}|| blacklist item.".format(itemname))
		else:
			await ctx.send("Okay, I deleted the message, but the item id you gave wasn't valid, so I can't give a reason. Sorry.")
			await message.author.send("Your message was deleted for violating the blacklist.")

	@blacklist.command(name="add")
	async def blacklist_add(self, ctx, squick: bool, local: bool, *, name):
		"""Add an item to your blacklist. Takes a true or false value for whether it's a squick, a true or false value for whether it is local to the current server, and an item name."""
		server = 0
		if local:
			server = ctx.guild.id
		self.cur.execute("INSERT INTO blacklist(id, owner, name, server, squick) VALUES (?,?,?,?,?)", (self.generate_id(False), ctx.author.id, name, server, squick))
		self.conn.commit()
		m1 = "squick" if squick else "trigger"
		m2 = "It will take effect just in this server." if local else "It will take effect in all your servers."
		await ctx.send("Okay, I added a {} with the name ||{}|| for you. {}".format(m1, name, m2))

	@blacklist.command(name="remove")
	async def blacklist_remove(self, ctx, item):
		"""Remove an item from your blacklist. Takes the 5-letter item id."""
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		items = self.cur.fetchall()
		if not items:
			await ctx.send("That's not a valid item id.")
			return
		if items[0][1] != ctx.author.id and not (not items[0][1] and ctx.author.guild_permissions.manage_guild):
			await ctx.send("That's not your blacklist item, you can't do that!")
			return
		self.cur.execute("DELETE FROM autodetect WHERE blacklist_item=?", (item,))
		self.cur.execute("DELETE FROM blacklist WHERE id=?", (item,))
		self.conn.commit()
		await ctx.send("Got it! I deleted that item for you.")
		await self.update_messages([g.id for g in self.bot.guilds if g.get_member(ctx.author.id)])

	@blacklist.command(name="rename")
	async def blacklist_rename(self, ctx, item, *, name):
		"""Rename an item in your blacklist. Takes the 5-letter item id and the new name."""
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		items = self.cur.fetchall()
		if not items:
			await ctx.send("That's not a valid item id.")
			return
		if items[0][1] != ctx.author.id and not (not items[0][1] and ctx.author.guild_permissions.manage_guild):
			await ctx.send("That's not your blacklist item, you can't do that!")
			return
		self.cur.execute("UPDATE blacklist SET name=? WHERE id=?", (name, item))
		self.conn.commit()
		await ctx.send("Got it! I renamed that item to {}.".format(name))
		await self.update_messages([g.id for g in self.bot.guilds if g.get_member(ctx.author.id)])

	@blacklist.command()
	async def toggle_public(self, ctx):
		"""Toggle whether your triggers are publicly viewable with m.blacklist user and m.blacklist info. Your squicks will always be public."""
		self.cur.execute("SELECT * FROM users WHERE id=?", (ctx.author.id,))
		pub = self.cur.fetchall()[0][1]
		if pub:
			self.cur.execute("UPDATE users SET blacklist_public=? WHERE id=?", (False, ctx.author.id))
			self.conn.commit()
			await ctx.send("Okay, I set your triggers to private again! They'll still be in effect, people just won't know who has them.")
		else:
			self.cur.execute("UPDATE users SET blacklist_public=? WHERE id=?", (True, ctx.author.id))
			self.conn.commit()
			await ctx.send("Okay, everyone can see which triggers belong to you now.")

	@blacklist.command(name="info")
	async def blacklist_info(self, ctx, item):
		"""Get information on a blacklist item (its name, id, whether it's a squick, whether it's server-local, who it belongs to if applicable, and any autodetect phrases associated with it. Takes the 5-letter item id."""
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		it = self.cur.fetchall()[0]
		self.cur.execute("SELECT * FROM autodetect WHERE blacklist_item=?", (item,))
		phrases = self.cur.fetchall()
		pub = False
		if it[1]:
			self.cur.execute("SELECT * FROM users WHERE id=?", (it[1],))
			pub = self.cur.fetchall()[0][1]
		autodetect_msg = "None"
		if phrases:
			autodetect_msg = ", ".join(["||{}|| (id: {})".format(p[1], p[0]) for p in phrases])
		own = "Private"
		if pub or it[4]:
			own = self.bot.get_user(it[1]).name
		await ctx.send("Here's some info about that!\n\nID: {}\nName: {}\nSquick: {}\nServer-Local: {}\nOwner: {}\nAutodetect Phrases: {}".format(it[0], it[2], bool(it[4]), bool(it[3]), own, autodetect_msg))

	@blacklist.command(name="user")
	async def blacklist_user(self, ctx, user: discord.User):
		"""View a user's blacklist. This includes their squicks, and their triggers if they are set to public."""
		self.cur.execute("SELECT * FROM users WHERE id=?", (user.id,))
		pub = self.cur.fetchall()[0][1]
		if pub or (user.id == ctx.author.id):
			self.cur.execute("SELECT * FROM blacklist WHERE owner=?", (user.id,))
		else:
			self.cur.execute("SELECT * FROM blacklist WHERE owner=? AND squick=?", (user.id, True))
		await ctx.send("Here's {}'s blacklist: {}".format(user.name, ", ".join(["||{}|| (id: {})".format(i[2], i[0]) for i in self.cur.fetchall()])))


	@blacklist.group()
	async def autodetect(self, ctx):
		"""Manage the blacklist's autodetect feature, which uses defined phrases to detect whether a blacklist item is being discussed. Autodetect will never take corrective action without user intervention."""
		if ctx.invoked_subcommand is None:
			await ctx.send("The autodetect subcommand has several further subcommands. Use m.help blacklist autodetect for more information.")

	@autodetect.command()
	@commands.check(has_manage_server)
	async def toggle(self, ctx):
		"""Toggle autodetect warnings on this server. Requires Manage Server."""
		self.cur.execute("SELECT * FROM servers WHERE id=?", (ctx.guild.id,))
		enabled = self.cur.fetchall()[0][1]
		if enabled:
			self.cur.execute("UPDATE servers SET autodetect=? WHERE id=?", (False, ctx.guild.id))
			self.conn.commit()
			await ctx.send("Okay, I'll stop looking for autodetect phrases.")
		else:
			self.cur.execute("UPDATE servers SET autodetect=? WHERE id=?", (True, ctx.guild.id))
			self.conn.commit()
			await ctx.send("Got it! From now on, I'll now automatically notify people when they post an autodetect phrase.")

	@autodetect.command()
	@commands.check(has_manage_server)
	async def warnings_channel(self, ctx, ch: discord.TextChannel):
		"""Set the channel where dismissed autodetect warnings are redirected. Should probably be a mod-only channel. Takes the channel as an argument. Requires Manage Server."""
		if ch.guild != ctx.guild:
			await ctx.send("Ooh, sneaky... that isn't in this server!")
			return
		self.cur.execute("UPDATE servers SET warnings_channel=? WHERE id=?", (ch.id, ctx.guild.id))
		await ctx.send("Alright, dismissed warnings will now be forwarded to {}.".format(ch.mention))

	@autodetect.command()
	@commands.check(has_manage_server)
	async def disable_phrase(self, ctx, phrase):
		"""Disable a specific autodetect phrase in the server. Takes the 5-letter phrase id. Requires Manage Server."""
		self.cur.execute("SELECT * FROM autodetect WHERE id=?", (phrase,))
		if not self.cur.fetchall():
			await ctx.send("That's not a valid autodetect phrase.")
			return
		self.cur.execute("SELECT * FROM autodetect_disabled WHERE server=? AND id=?", (ctx.guild.id, phrase))
		if self.cur.fetchall():
			await ctx.send("That phrase is already disabled on this server.")
			return
		self.cur.execute("INSERT INTO autodetect_disabled(server,id) VALUES (?,?)", (ctx.guild.id, phrase))
		self.conn.commit()
		await ctx.send("Got it! I disabled that phrase for you.")

	@autodetect.command()
	@commands.check(has_manage_server)
	async def enable_phrase(self, ctx, phrase):
		"""Re-enable a specific autodetect phrase in the server. Takes the 5-letter phrase id. Requires Manage Server."""
		self.cur.execute("SELECT * FROM autodetect WHERE id=?", (phrase,))
		if not self.cur.fetchall():
			await ctx.send("That's not a valid autodetect phrase.")
			return
		self.cur.execute("SELECT * FROM autodetect_disabled WHERE server=? AND id=?", (ctx.guild.id, phrase))
		if not self.cur.fetchall():
			await ctx.send("That phrase isn't disabled on this server.")
			return
		self.cur.execute("DELETE FROM autodetect_disabled WHERE server=? AND id=?", (ctx.guild.id, phrase))
		self.conn.commit()
		await ctx.send("Got it! I re-enabled that phrase for you.")

	@autodetect.command(name="add")
	async def autodetect_add(self, ctx, item, *, phrase):
		"""Add an autodetect phrase to a blacklist item. Takes the 5-letter item id, and the phrase."""
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (item,))
		items = self.cur.fetchall()
		if not items:
			await ctx.send("That's not a valid blacklist item!")
			return
		if items[0][1] != ctx.author.id and not (not items[0][1] and ctx.author.guild_permissions.manage_guild):
			await ctx.send("You don't own that blacklist item!")
			return
		self.cur.execute("INSERT INTO autodetect(id,phrase,blacklist_item) VALUES (?,?,?)", (self.generate_id(True), phrase, item))
		self.conn.commit()
		await ctx.send("Okay! Phrase ||{}|| added to blacklist item ||{}||. I'll watch out for it!".format(phrase, items[0][2]))

	@autodetect.command(name="remove")
	async def autodetect_remove(self, ctx, phrase):
		"""Remove an autodetect phrase from a blacklist item. Takes the 5-letter phrase id."""
		self.cur.execute("SELECT * FROM autodetect WHERE id=?", (phrase,))
		phrases = self.cur.fetchall()
		if not phrases:
			await ctx.send("That's not a valid phrase id.")
			return
		self.cur.execute("SELECT * FROM blacklist WHERE id=?", (phrases[0][2],))
		items = self.cur.fetchall()
		if items[0][1] != ctx.author.id and not (not items[0][1] and ctx.author.guild_permissions.manage_guild):
			await ctx.send("That's not your autodetect phrase, you can't do that!")
			return
		self.cur.execute("DELETE FROM autodetect WHERE id=?", (phrase,))
		self.conn.commit()
		await ctx.send("Got it! I deleted that phrase for you.")


	@commands.Cog.listener()
	async def on_ready(self):
		self.cur.execute("SELECT * FROM servers")
		loaded = [g[0] for g in self.cur.fetchall()]
		for guild in self.bot.guilds:
			if not (guild.id in loaded):
				self.cur.execute("INSERT INTO servers(id,autodetect,warnings_channel,blacklist_channel) VALUES(?,?,?,?)", (guild.id, False, 0, 0))
		self.cur.execute("SELECT * FROM users")
		loaded = [g[0] for g in self.cur.fetchall()]
		for user in self.bot.users:
			if not (user.id in loaded):
				self.cur.execute("INSERT INTO users(id,blacklist_public) VALUES(?,?)", (user.id, False))
		self.conn.commit()

	@commands.Cog.listener()
	async def on_member_join(self, member):
		self.cur.execute("SELECT * FROM users WHERE id=?", (member.id,))
		if not self.cur.fetchall():
			self.cur.execute("INSERT INTO users(id,blacklist_public) VALUES(?,?)", (user.id, False))
		await self.update_messages([member.guild.id])

	@commands.Cog.listener()
	async def on_member_remove(self, member):
		await self.update_messages([member.guild.id])

	@commands.Cog.listener()
	async def on_message(self, message):
		if isinstance(message.channel, discord.DMChannel):
			return
		self.cur.execute("SELECT * FROM servers WHERE id=?", (message.guild.id,))
		servers = self.cur.fetchall()
		if not servers[0][1]:
			return
		phrases = self.get_phrases(message.guild.id)
		newmsg = self.spoiler_re.sub("", message.content)
		pattern = "|".join([re.escape(p) for p in phrases])
		matches = re.findall(pattern, newmsg)
		if not matches:
			return
		violations = []
		for m in matches:
			self.cur.execute("SELECT * FROM autodetect WHERE phrase=?", (m,))
			violations += self.cur.fetchall()
		items = []
		for v in violations:
			self.cur.execute("SELECT * FROM blacklist WHERE id=?", (v[2],))
			items += self.cur.fetchall()
		tosend = "Oops! I think a message you just sent violated the blacklist! If I'm right, you can reply with 'delete' to automatically delete the message. If I'm wrong, simply reply 'dismiss'!\n\n"
		if len(matches) == 1:
			tosend += "The phrase I found was {} (id: `{}`). That's associated with the item {} (id: `{}`).".format(matches[0], violations[0][0], items[0][2], items[0][0])
		else:
			tosend += "Phrases I found: "
			for i in range(len(matches)):
				tosend += "\n{} (id: `{}`) [trigger: {}, id: `{}`]".format(matches[i], violations[i][0], items[i][2], items[i][0])
		await message.author.send(tosend)
		def check(m):
			return m.content == "delete" or m.content == "dismiss"
		msg = None
		try:
			msg = await self.bot.wait_for('message', check=check, timeout=30)
		except asyncio.TimeoutError:
			if servers[0][2]:
				await self.bot.get_channel(servers[0][2]).send("{} timed out on a warning for this message: {}\n\nIf you think that was an error, use m.blacklist strike.".format(message.author.name, message.jump_url))
			await message.author.send("I automatically dismissed the warning.")
			return
		if msg.content == "delete":
			await message.delete()
			await message.author.send("Alright! I deleted the message for you. Thanks for cooperating!")
		else:
			if servers[0][2]:
				await self.bot.get_channel(servers[0][2]).send("{} dismissed a warning for this message: {}\n\nIf you think that was an error, use m.blacklist strike.".format(message.author.name, message.jump_url))
			await message.author.send("Alright, no worries! I'm glad everything's okay.")

	@commands.Cog.listener()
	async def on_guild_join(self, guild):
		self.cur.execute("INSERT INTO servers(id,autodetect,warnings_channel,blacklist_channel) VALUES(?,?,?,?)", (guild.id, False, 0, 0))
		self.conn.commit()


def setup(bot):
	bot.add_cog(Blacklist(bot))