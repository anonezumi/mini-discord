from discord.ext import commands
import sqlite3

class Mini(commands.Bot):
	def __init__(self):
		super().__init__(command_prefix="m.")
		self.conn = sqlite3.connect("data.db")
		self.cur = self.conn.cursor()
		for ext in ["admin", "logs", "util", "blacklist"]:
			self.load_extension(ext)

bot = Mini()
bot.run(open("token.txt").read())