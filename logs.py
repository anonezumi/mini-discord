import asyncio
from discord.ext import commands
import discord

class Logs(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.conn = bot.conn
		self.cur = bot.cur

	async def cog_check(self, ctx):
		return (not isinstance(ctx.channel, discord.DMChannel)) and ctx.author.guild_permissions.manage_guild

	async def cog_command_error(self, ctx, error):
		if isinstance(error, commands.CheckFailure):
			await ctx.send("Sorry, you have to have Manage Server permissions to do that.")
		else:
			await ctx.send("Oops, there was an error:\n\n" + str(error))


	async def log_message(self, message, logs):
		for log in logs:
			ch = await self.bot.fetch_channel(log[2])
			webhooks = await ch.webhooks()
			webhook = None
			for w in webhooks:
				if w.user == self.bot.user:
					webhook = w
					break
			await webhook.send(message.content, username=message.author.name,
				avatar_url=message.author.avatar_url, files=[await a.to_file() for a in message.attachments])

	@commands.group(name="log")
	async def log_command(self, ctx):
		"""Commands for setting up logging of certain users' messages. Originally created for TWOWs. When logs are set up, messages from anyone with a Host role in the source channel will be copied to the log channel via webhooks. If you want to prevent an individual message being logged, start it with double open parentheses "((". Requires Manage Server."""
		if ctx.invoked_subcommand is None:
			await ctx.send("Tell me what you want to do! Log commands include list, setup, and delete. Oh, and you can use m.help log for more info.")

	@log_command.command(name="setup")
	async def log_setup(self, ctx, source: discord.TextChannel, log: discord.TextChannel):
		"""Set up a log. Source is the channel to copy from, log is the channel to copy to."""
		if source.guild != ctx.guild or log.guild != ctx.guild:
			await ctx.send("Ooh, sneaky... that isn't in this server!")
			return
		await log.create_webhook(name="Mini Log")
		await ctx.send("Okay! Messages from {} from users with the Host role will now be logged via webhooks into {}.".format(source.mention, log.mention))
		self.cur.execute("INSERT INTO logs(source,log) VALUES(?,?)", (source.id, log.id))
		self.conn.commit()

	@log_command.command(name="list")
	async def log_list(self, ctx):
		"""Gets a list of logs on this server and their ids."""
		chids = [c.id for c in ctx.guild.text_channels]
		self.cur.execute("SELECT * FROM logs WHERE source IN ({})".format(",".join(["?"]*len(chids))), chids)
		active_logs = self.cur.fetchall()
		if not active_logs:
			await ctx.send("Looks like this server doesn't have any logs right now.")
			return
		await ctx.send("Okay! Here's a list of this server's logs, including log ids:\n\n" +
			"\n".join(["[ID: {}] <#{}> to <#{}>".format(log[0], log[1], log[2]) for log in active_logs]))

	@log_command.command(name="delete")
	async def log_delete(self, ctx, log_id: int):
		"""Delete a log with its id. You can find a log's id using m.log list."""
		self.cur.execute("SELECT * FROM logs WHERE id=?", (log_id,))
		try:
			log = self.cur.fetchall()[0]
		except IndexError:
			await ctx.send("That log doesn't even exist, how do you expect me to delete it?")
			return
		if log[1] in [c.id for c in ctx.guild.text_channels]:
			await ctx.send("Alright, I'm deleting the log from <#{}> to <#{}>.".format(log[1], log[2]))
			self.cur.execute("DELETE FROM logs WHERE id=?", (log_id,))
			self.conn.commit()
		else:
			await ctx.send("That log isn't on this server.")

	@commands.group()
	async def joinlog(self, ctx):
		"""Commands for setting up logging of joins and leaves. Requires Manage Server."""
		if ctx.invoked_subcommand is None:
			await ctx.send("Tell me what you want to do! Joinlog commands include joinmessage, leavemessage, enable, and disable. Use m.help joinlog for more info.")

	@joinlog.command(name="enable")
	async def joinlog_enable(self, ctx, ch: discord.TextChannel):
		"""Enables the joinlog in the specified channel. If already enabled in another channel, joinlog will switch channels."""
		self.cur.execute("SELECT * FROM joinlogs WHERE server=?", (ctx.guild.id,))
		joinlogs = self.cur.fetchall()
		if joinlogs != []:
			if joinlogs[0][1] == ch.id:
				await ctx.send("Looks like joinlog is already enabled in that channel! If it's not working, make sure I can talk there.")
				return
			await ctx.send("Okay, I'll switch from sending messages in <#{}> to sending them in <#{}>.".format(joinlogs[0][1], ch.id))
			self.cur.execute("UPDATE joinlogs SET channel=? WHERE server=?", (ch.id, ctx.guild.id))
			self.conn.commit()
			return
		self.cur.execute("INSERT INTO joinlogs(server, channel, joinmsg, leavemsg) VALUES(?,?,?,?)", (ctx.guild.id, ch.id, "Welcome {mention} to {server}!", "Goodbye, {name}. We will miss you."))
		self.conn.commit()
		await ctx.send("Got it, I'll start logging joins and leaves in <#{}>. Here's a pro tip: you can customize messages using m.joinlog joinmessage and m.joinlog leavemessage.".format(ch.id))

	@joinlog.command(name="disable")
	async def joinlog_disable(self, ctx):
		"""Disables joinlog in this server. Note that this will delete your message settings."""
		self.cur.execute("DELETE FROM joinlogs WHERE server=?", (ctx.guild.id,))
		self.conn.commit()
		await ctx.send("Got it. I won't log joins and leaves here.")

	@joinlog.command()
	async def leavemessage(self, ctx, *, msg):
		"""Sets the message I'll send when someone leaves. You can use {mention}, {name}, and {server} in the message and I'll substitute those for you."""
		self.cur.execute("UPDATE joinlogs SET leavemsg=? WHERE server=?", (msg, ctx.guild.id))
		self.conn.commit()
		await ctx.send("Okay! Now, whem someone leaves, I'll say \"{}\".".format(msg))

	@joinlog.command()
	async def joinmessage(self, ctx, *, msg):
		"""Sets the message I'll send when someone joins. You can use {mention}, {name}, and {server} in the message and I'll substitute those for you."""
		self.cur.execute("UPDATE joinlogs SET joinmsg=? WHERE server=?", (msg, ctx.guild.id))
		self.conn.commit()
		await ctx.send("Okay! Now, whem someone joins, I'll say \"{}\".".format(msg))

	@commands.Cog.listener()
	async def on_member_join(self, member):
		self.cur.execute("SELECT * FROM joinlogs WHERE server=?", (member.guild.id,))
		joinlogs = self.cur.fetchall()
		if joinlogs:
			jl = joinlogs[0]
			ch = await self.bot.fetch_channel(jl[1])
			await ch.send(jl[2].format(mention=member.mention, name=member.name, server=member.guild.name))

	@commands.Cog.listener()
	async def on_member_remove(self, member):
		self.cur.execute("SELECT * FROM joinlogs WHERE server=?", (member.guild.id,))
		joinlogs = self.cur.fetchall()
		if joinlogs:
			jl = joinlogs[0]
			ch = await self.bot.fetch_channel(jl[1])
			await ch.send(jl[3].format(mention=member.mention, name=member.name, server=member.guild.name))

	@commands.Cog.listener()
	async def on_message(self, message):
		if message.author.bot or isinstance(message.channel, discord.DMChannel):
			return
		if "host" in [r.name.lower() for r in message.author.roles] and message.content[0:2] != "((":
			self.cur.execute("SELECT * FROM logs WHERE source=?", (message.channel.id,))
			logs = self.cur.fetchall()
			if logs:
				await self.log_message(message, logs)

def setup(bot):
	bot.add_cog(Logs(bot))